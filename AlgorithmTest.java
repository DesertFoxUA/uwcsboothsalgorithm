import java.util.Scanner;

public class AlgorithmTest{

    // TODO: Implement Restrictions
    public static void main(String args[]){
        Scanner sc = new Scanner(System.in);
        int mCand, 
             multiplier;
             
        BoothsAlgorithm ba;

        boolean contFlag = true;

        System.out.println("Enter Multicand: ");
        mCand = sc.nextInt();

        System.out.println("Enter multiplier: ");
        multiplier = sc.nextInt();

        ba = new BoothsAlgorithm(mCand, multiplier);
        ba.start();

        // Uncomment the line bellow to test functions.
        //ba.test();
    }

    public static boolean checkForRange(int input){
        if(input <= 128 || input >= -128){
            return true;
        }

        return false;
    }
}
