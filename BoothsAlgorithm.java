import java.lang.Math;
import java.util.Arrays.*;
import java.util.Arrays;

public class BoothsAlgorithm{
    int remainder = 0,
         stepCounter = 0,
         mcandDecimal, 
         multDecimal;
    
    int[] mcand = new int[8];
    int[] product = new int[16];

    public BoothsAlgorithm(int _mcand, int _mult){
        mcand = convertToBinary(_mcand, mcand.length);
        product = setProduct(convertToBinary(_mult, mcand.length));
        mcandDecimal = _mcand;
        multDecimal = _mult;
    }

    public void start(){
        System.out.println("\n\n\n------------------------------------------------------");
        System.out.println("\t Booths Algorithm");
        System.out.println("------------------------------------------------------");
        System.out.println("Inputs - "+ mcandDecimal + " and " + multDecimal);
        System.out.println("------------------------------------------------------");
        printStep("Initial");
        stepCounter++;

        for(int i = 0; i < mcand.length; i++){
            arithmeticStep(product[product.length - 1] + "" + remainder);
            product = ARS(product);
            printStep("ARS");
            stepCounter++;
        }

        stepCounter = 0;
        System.out.println("------------------------------------------------------");
        System.out.format("| Binary->Decimal: %1$s | Decimal -> Decimal: %2$s \n", convertBinaryToDecimal(product), (mcandDecimal * multDecimal));
        System.out.println("------------------------------------------------------");
    }

    private void arithmeticStep(String _lastTwoProductBits){
        switch(_lastTwoProductBits){
            case "01":
                product = addMCand(product, mcand);
                printStep("Add MCand");
                break;
            case "10":
                product = subMCand(product, mcand);
                printStep("Sub MCand");
                break;
            default:
                printStep("No OP");
                return;
        }
    }

    private int[] addMCand(int[] _product, int[] _mcand){
        int carryOver = 0;

        for(int i = _mcand.length - 1; i >= 0; i--){
            int sum = _product[i] + _mcand[i] + carryOver;
            _product[i] = sum % 2;
            carryOver = sum / 2;
        }

        return _product;
    }

    private int[] subMCand(int[] _product, int[] _mcand){
        // Adding by subtracting
        return addMCand(_product, twosCompliment(_mcand, _mcand.length));
    }

    private int[] ARS(int[] _binaryArray){
        int[] binaryArrayAfterARS = _binaryArray;
        remainder = (byte)_binaryArray[_binaryArray.length - 1];
        
        System.arraycopy(_binaryArray, 0, binaryArrayAfterARS, 1, _binaryArray.length - 1);

        return binaryArrayAfterARS;
    }

    private int[] convertToBinary(int _decimalNumber, int _bits){
        /** We do not know the length of the interger.
            To resolve this issue for any size number we need pass
            the amount of bits we want out.

            Exammple:   _bits = 8;
                        int.length = _bits;
                        >> 00000000
         **/

        // Instanciate output array
        int[] binaryArray = new int[_bits];
        int positiveDecimalNumber = Math.abs(_decimalNumber);

        // Using divide by 2 algorithm to get a binary number.
        for(int i = _bits - 1; i >= 0; i--){
            binaryArray[i] = positiveDecimalNumber % 2;
            positiveDecimalNumber = (int)Math.floor(positiveDecimalNumber / 2);
        }

        if(checkIfNegative(_decimalNumber)){
             binaryArray = twosCompliment(binaryArray, _bits);
         }

        // return
        return binaryArray;
    }

    private boolean checkIfNegative(int _decimalValue){
        if(_decimalValue < 0){
            return true;
        }

        return false;
    }

    private int[] setProduct(int[] _binaryArray){
        int[] productArray = new int[product.length];
        System.arraycopy(_binaryArray, 0, productArray, _binaryArray.length, _binaryArray.length);
        return productArray;
    }

    private int[] twosCompliment(int[] _binaryArray, int _bits){
        int[] negatedArray = new int[_bits];

        for(int i = 0; i < _bits; i++ ){
            negatedArray[i] = (_binaryArray[i] + 1) % 2;
        }

        negatedArray = addMCand(negatedArray, convertToBinary(1, _bits));

        return negatedArray;
    }

    public String printBinaryString(int[] _binaryArray){
        String binaryString = "";

        // Iterate through the array and concatinate with the output string.
        for(int i = 0; i < _binaryArray.length; i++){

            if(i == (int)Math.floor((_binaryArray.length / 2))){
                binaryString += " ";
            }

            binaryString += _binaryArray[i];
        }

       return binaryString;
    }

    public int convertBinaryToDecimal(int[] _binaryArray){
        int total = 0;
        boolean isNegative = false;

        if( _binaryArray[0] == 1){
            _binaryArray = twosCompliment(_binaryArray, _binaryArray.length);
            isNegative = true;
        }

        for(int i = 0; i < _binaryArray.length; i++){
            total += _binaryArray[i] * Math.pow(2, (_binaryArray.length - 1 ) - i);
        }

        return isNegative ? (-1) * total : total;
    }
    
    private void printStep(String _stepName){
        System.out.format("| %1$s | %2$10s | %3$s | %4$s | %5$s |\n", stepCounter, _stepName, printBinaryString(mcand), printBinaryString(product), remainder);
    }

    // Testing functions
    public void test(){
        System.out.println("\n*********************TEST*************************\n");

        BoothsAlgorithm ba;

        for(int i = 0; i < 64; i++){
            ba = new BoothsAlgorithm(i, (-1) * i);
            ba.start();
        }

        System.out.println("\n*********************END TEST*********************\n");
    }
}